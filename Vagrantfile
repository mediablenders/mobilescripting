require 'json'
require 'yaml'

VAGRANTFILE_API_VERSION = "2"

homesteadYamlPath = File.expand_path("Homestead.yaml")
afterScriptPath = File.expand_path("scripts/after.sh")
mongoScriptPath = File.expand_path("scripts/mongo.sh")
mongoHHVMExtension = File.expand_path("scripts/extensions/hhvm/mongo.so")
aliasesPath = File.expand_path("aliases")

require_relative 'scripts/homestead.rb'

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
	if File.exists? aliasesPath then
		config.vm.provision "file", source: aliasesPath, destination: "~/.bash_aliases"
	end

    # Make sure the mongo.so extension is copied into a temp location with file provisioner, move it into place using the shell provisioner
    if File.exists? mongoHHVMExtension then
        config.vm.provision "file", source: mongoHHVMExtension, destination: "~/mongo.so"
    end

	Homestead.configure(config, YAML::load(File.read(homesteadYamlPath)))

	if File.exists? afterScriptPath then
		config.vm.provision "shell", path: afterScriptPath
        config.vm.provision "shell", path: mongoScriptPath
	end
end
