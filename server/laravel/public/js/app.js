var NewsApp = angular.module('NewsApp', ['ngRoute']);
	
NewsApp.controller('NewsController', ['$scope', function($scope) {

	$scope.newsCategory = [
		{title: 'First nieuws item', slug: "first-news-item"},
		{title: 'Second nieuws item', slug: "second-news-item"}
	];

}]);

NewsApp.controller('NewsControllerDetail', ['$scope', function($scope) {
  
	$scope.item =  {
		title: 'First nieuws item',
		content: 'Content'
	};

}])

NewsApp.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
    	when('/', {
    		templateUrl: 'views/home.html',
        	controller: 'NewsController'
    	}).
    	when('/news/new', {
    		templateUrl: 'views/newsForm.html',
        	controller: 'NewsControllerDetail'
    	}).
    	when('/news/:newsSlug', {
    		templateUrl: 'views/single.html',
        	controller: 'NewsControllerDetail'
    	})  
		.otherwise({
			redirectTo: '/'
		});
  }]);