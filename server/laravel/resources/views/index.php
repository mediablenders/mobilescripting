<!DOCTYPE html>
<html lang="en" ng-app="NewsApp">
<head>
	<meta charset="UTF-8">
	<title>App</title>
	<link rel="stylesheet" href="http://mobilescripting.local/css/main.css">
	<script src="http://mobilescripting.local/js/libraries/angular.min.js"></script>
	<script src="http://mobilescripting.local/js/libraries/angular-route.min.js"></script>
	<script src="http://mobilescripting.local/js/app.js"></script>
</head>
<body>
	<div>
		<ng-include src="'views/header.html'"></ng-include>
	</div>
	<div ng-view></div>
</body>
</html>