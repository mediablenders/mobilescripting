<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use Moloquent as Model;

class User extends Model  {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $collection = 'news';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['title', 'content', 'slug'];

	private $rules = array(
		'title' => 'required',
		'content' => 'required',
		'slug'  => 'required:unique',
	);

	private $validator;
	private $validation_errors;

	/**
	 * Add stuff to the model in it's boot phase such as an observer
	 */
	public static function boot() {
		parent::boot();
		//Product::observe(new ProductObserver);
	}
	public function validate($data) {
		$validator = Validator::make($data, $this->rules);
		if ($validator->fails()) {
			$this->validation_errors = $validator->messages();
			return false;
		}
		return true;
	}
	public function errors() {
		return $this->validation_errors;
	}

	/**
	 * Get single product based on slug
	 *
	 * @param string $slug
	 * @return array
	 */
	public function find($slug){
		return $this->model
		 ->where('slug', $slug)->get();
	}

}
