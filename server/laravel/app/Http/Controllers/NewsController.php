<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class NewsController extends Controller {

	function index(){
		$news = News::all();
		return $this->newJSONResponse($news, 200);
	}

	function store(){
		
	}

	function show($slug){
		return News::find($slug);
	}

	function update($id){

	}

	function destroy(){
		
	}

}