

module.exports = function (grunt) {
    require('load-grunt-tasks')(grunt);
    require('time-grunt')(grunt);

    var config = {
        app: 'server/laravel/public/',
        resource: 'server/laravel/resources/assets/',
        dist: 'dist'
    };

    grunt.initConfig({
        config: config,

        pkg: grunt.file.readJSON('package.json'),

        // Watch configuration
        // Define all the files that you need watched and which tasks to run on it
        watch: {
            sass: {
               files: ['<%= config.resource %>/sass/{,*/}*.{scss,sass}'],
               tasks: 'compass:dev'
            }
        },

        // Compiling sass. Different tasks for dist and dev.
        compass: {
            dev: {
                options: {
                   sassDir: '<%= config.resource %>/sass/',
                   cssDir: '<%= config.app %>/css/'
                },
            },
            dist: {
                options: {
                   sassDir: '<%= config.resource %>/sass/',
                   cssDir: '<%= config.dist %>/css/'
                },
            },
        }
    });
      
    // grunt.registerTask('serve', function (target) {
    //     if(target === 'dist'){
    //         return grunt.task.run(['build', 'connect:dist:keepalive']);
    //     }
    //     grunt.task.run([
    //         'clean:server',
    //         'compass:dev',
    //         'connect:livereload',
    //         'watch'
    //     ]);
    // });
    // grunt.registerTask('test', function (target){
    //     console.log('testing')
    //     if(target !== 'watch'){

    //     }
    //     grunt.task.run([
    //         'connect:test'
    //     ]);
    // });
    
    // grunt.registerTask('build',[
    //     'clean:dist',
    //     'compass:dist',
    //     'cssmin',
    //     'copy'
    // ]);
    // grunt.registerTask('default', [
    //     'test',
    //     'build'
    // ]);
};