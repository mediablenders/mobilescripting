#!/usr/bin/env bash

mkdir -p ~/.homestead

cp src/stubs/Homestead.yaml ./Homestead.yaml
cp src/stubs/after.sh ./after.sh
cp src/stubs/aliases ./aliases

echo "Homestead initialized!"
