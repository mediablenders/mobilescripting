#!/usr/bin/env bash

echo ">>> Installing MongoDB"

# Get key and add to sources
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10
echo 'deb http://downloads-distro.mongodb.org/repo/ubuntu-upstart dist 10gen' | sudo tee /etc/apt/sources.list.d/mongodb.list

# Update
sudo apt-get update

# Install MongoDB
# -qq implies -y --force-yes
sudo apt-get install -qq mongodb-org

# Make MongoDB connectable from outside world without SSH tunnel
if [ $1 == "true" ]; then
    # enable remote access
    # setting the mongodb bind_ip to allow connections from everywhere
    sed -i "s/bind_ip = */bind_ip = 0.0.0.0/" /etc/mongod.conf
fi

# Test if PHP is installed
php -v > /dev/null 2>&1
PHP_IS_INSTALLED=$?

if [ $PHP_IS_INSTALLED -eq 0 ]; then
    # install dependencies
    sudo apt-get -y install php-pear php5-dev

    # install php extencion
    echo "no" > answers.txt
    sudo pecl install mongo < answers.txt
    rm answers.txt

    # add extencion file and restart service
    echo 'extension=mongo.so' | sudo tee /etc/php5/mods-available/mongo.ini

    ln -s /etc/php5/mods-available/mongo.ini /etc/php5/fpm/conf.d/mongo.ini
    ln -s /etc/php5/mods-available/mongo.ini /etc/php5/cli/conf.d/mongo.ini
    sudo service php5-fpm restart


    # make sure HHVM has the right driver for mongodb by moving it from its temp location
    sudo mv /home/vagrant/mongo.so /usr/lib/hhvm

    cd ~
    git clone git://github.com/mongodb/libbson.git
    cd libbson/
    ./autogen.sh
    make
    sudo make install

    block="; php options
    session.save_handler = files
    session.save_path = /var/lib/php5
    session.gc_maxlifetime = 1440

    ; hhvm specific
    hhvm.dynamic_extension_path = /usr/lib/hhvm
    hhvm.dynamic_extensions[mongo] = mongo.so

    hhvm.log.level = Warning
    hhvm.log.always_log_unhandled_exceptions = true
    hhvm.log.runtime_error_reporting_level = 8191
    hhvm.mysql.typed_results = false"

    echo "$block" > "/etc/hhvm/php.ini"

    sudo service hhvm restart
fi

